<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Post;
class AuthController extends Controller
{
  
        public function login (){
            return view('login');
        }

        public function register (){
            return view('register');
        }

        public function profile(){
            return view('profile');
            
        }


        public function store(Request $request){

            $request->validate([
                'judul' => 'required|unique:pertanyaan',
                'isi' => 'required'
            ]);
    
    
            $query = DB::table('pertanyaan')->insert([
                'judul'=> $request['judul'],
                'isi' => $request['isi']
                ]);
            return redirect ('pertanyaan')->with('success', 'Pertanyaaan berhasil di posting!!!');
            }



            
}

