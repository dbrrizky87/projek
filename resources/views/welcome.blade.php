@extends('adminlte.master-nav')

@section('content')
@forelse ($posts as $key => $item)
    

<div class="container content">
    <div class="row">
      <div div class="card-body mt-3">
      <div class="tab-content">
      <div class="active tab-pane" id="activity">
        <!-- Post -->
        <div class="post">
          <div class="user-block">
            <img class="img-circle img-bordered-sm" src="/adminlte/dist/img/user1-128x128.jpg" alt="user image">
            <span class="username">
              <a href="#">Jonathan Burke Jr.</a>
              <div class="btn-group float-right" role="group">
                <a class="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-v"></i>
                </a>
                <form action="{{ action('PostController@destroy',['id'=>$item->id]) }}" method="POST">
                  {{ csrf_field() }}
                  {{ method_field('DELETE') }}
                <div class="dropdown-menu" id="show" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModal{{$key + 1}}" id="edit"> <i class="fas fa-pen mr-2"></i> Edit</a>
                  <a class="dropdown-item " href="" onclick="return confirm('Confirm delete?')" type="submit" id="delete"> <i class="far fa-trash-alt mr-3"  ></i>Delete</a> 
                </div>
                <form>
                <form action="/" action="POST">
                @csrf
                @method('PUT')
                <div class="modal fade" id="exampleModal{{$key + 1}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-scrollable modal-transparent modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Update Status</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                       <textarea class="ml-2" name="status" class="form-control" style="min-width: 100%">{{$item -> status}}</textarea>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
              </div>  
            </span>
            <span class="description">Shared publicly - {{$item -> created_at}}</span>
          </div>
          <!-- /.user-block -->
          <p>
            {{$item -> status}}
          </p>

          <hr>  
            <div class="panel-group">
              <div class="panel panel-default">
                <div class="panel-heading   ">
                   <p class="link-black text-sm mr-3 liketoggle" id="countButton"><i class="far fa-thumbs-up mr-1"></i>Like <span id="displayCount">(0)</span> </p>
                    <a class="link-black text-sm" data-toggle= "collapse" href="#collapse{{$key + 1}}"> <i class="far fa-comments mr-1"></i> Comments (5)</a>
                </div>
                <hr>
                <div id="collapse{{$key + 1}}" class="panel-collapse collapse mt-3 ">
                  <ul class="list-group ans ">
                    <span class="username">
                      <a href="#">Jonathan Burke Jr.</a>  
                    </span>
                    <span class="description">3 minute ago</span>
                     <p>Lorem ipsum represents a long-held tradition for designers,
                      typographers and the like. Some people hate it and argue for
                      its demise, but others ignore the hate as they create awesome
                      tools to help create filler text for everyone from bacon lovers
                      to Charlie Sheen fans.</p>
                  </ul>
                  <hr>
                  <ul class="list-group ans ">
                    <span class="username">
                      <a href="#">Jonathan Burke Jr.</a>  
                    </span>
                    <span class="description">3 minute ago</span>
                     <p>Lorem ipsum represents a long-held tradition for designers.</p>
                  </ul>
                  <hr>
                </div>
                
              </div>
            </div>
          <form class="form-horizontal">
            <div class="input-group input-group-sm mb-0">
              <input class="form-control form-control-sm" placeholder="Response">
              <div class="input-group-append">
                <button type="submit" class="btn btn-danger">Send</button>
              </div>
            </div>
          </form>
        </div>
        <!-- /.post -->
      </div>      
    </div>
    <!-- /.tab-content -->
  </div>
  </div>
</div>
<hr class="hr-split">



@empty
@endforelse
  @endsection