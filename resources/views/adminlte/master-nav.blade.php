<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Stack |  Home</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Main Style -->
  <link rel="stylesheet" href="{{asset('/adminlte/css/main.css')}}">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('/adminlte/plugins/fontawesome-free/css/all.min.css')}}">
  
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('/adminlte/dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
<style>
    * {
    margin: 0;
    padding: 0;
}

.navbar {
  background: linear-gradient(to bottom, #212529 0%, #003366 100%)!important;
  box-shadow: #1876d4 0px 0px 15px;
}

ul li a , .navbar-brand , h2{
  color: rgb(146, 171, 207)!important;
}
ul li a:hover  {
  color: rgb(17, 236, 229)!important;
}

.navbar-brand:hover  {
  color: rgb(17, 236, 229)!important;
}

.content-wrapper {
    margin-left: 0px !important;
    margin-top: 50px;
    background-color: rgb(26, 24, 24);
}

.content {
    width: 70%;
    background-color:#212529;
    max-width: 700px;
}

footer {
    margin: 0px !important;
    background: linear-gradient(to bottom, #212529 0%, #003366 100%)!important;

}

#collapse1 {
    background-color: #212529!important ;
}

.ans {
  background-color: #32383d;
  width: 90%;
  margin: 0 auto;
  padding: 20px;
  border-radius: 10px;
}

p {
  color: white
}

#countButton {
  cursor: pointer;
  justify-items: space-between;
}

.flex-container {
  display: flex;
}

hr {
  border-top: 1px solid rgb(107, 106, 106);
}
.hr-split {
  border-top: 1px solid rgb(26, 24, 24);
}

/* profile */
.top-cont {
  height: 600px!important;
  background: linear-gradient(to bottom, #212529 0%, #003366 100%);
  text-align: center;
}

.top-cont .bg-prof {
  
  height: 330px;
  width: 700px;
  border-radius: 10px;
  box-shadow: #1876d4 0px 0px 15px;
  margin-top:-50px;
}

.jumbotron .pho-prof img{
 align-items: center;
 box-shadow: #1876d4 1px 1px 10px;

}


h3 {
  margin-top: 40px;
  padding: 15px;
}

.input-col {

  font-weight: bold; 
 font-size: 12px; 
 background-color:#212529!important;

}
.liketoggle i.fas {
  color: blue;
}

.blue {
  color: blue;
}

/* Modal */
.modal-transparent .modal-content {
  background: rgba(40,36,47,.85)
}
textarea {
  background: rgba(50, 45, 59, 0.85);
  color: white;

}

  </style>
</head>
<body>
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  @include('adminlte.partials.top-navbar')
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   @yield('content')
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <center> <strong>Copyright &copy; 2020 <a href="http://adminlte.io">Stack For Us</a>.</strong> All rights
    reserved.</center>
  </footer>

  <!-- Control Sidebar -->
 
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->

<script src="{{asset('/adminlte/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('/adminlte/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('/adminlte/dist/js/demo.js')}}"></script>


<script>



$(".liketoggle").click(function() {
  $(this).find( 'i' ).toggleClass( "far fas" );
  
 });

    var count = 0;
    var countButton = document.getElementById("countButton");
      countButton.onclick = function(){
      if(count < 1){
      count++;
      displayCount.innerHTML = count;
      }else {
      count--;
      displayCount.innerHTML = count;
      }
    }   

    $('#edit').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) 
      var status = button.data('status') 
      var description = button.data('mydescription') 
      var status_id = button.data('id') 
      var modal = $(this)
      modal.find('.modal-body #status').val(title);
      modal.find('.modal-body #status_id').val(status_id);
      })


  $('#delete').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) 
      var status_id = button.data('id') 
      var modal = $(this)
      modal.find('.modal-body #status_id').val(id);
      })

</script>

</body>
</html>
