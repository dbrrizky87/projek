
 <!--Navbar-->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar">

            <div class="container">

                <!-- Navbar brand -->
                <a class="navbar-brand font-weight-bold " href="#">STACK FOR US</a>

                <!-- Collapse button -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
                    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!-- Collapsible content -->
                <div class="collapse navbar-collapse" id="basicExampleNav">

                    <!-- Links -->
                  <div class=" ml-5 mr-auto smooth-scroll">
                    
                  </div>
                    <!-- Links -->

                    <!-- Social Icon  -->
                    <ul class=" navbar-nav  nav-flex-icons">  
                      <li class="nav-item">
                        <a class="link-black nav-link" href="/">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="link-white nav-link " href="/profile">Profile</a>
                    </li>                    
                        <li class="nav-item">
                            <a class="link-black nav-link" href="/login"> Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="link-black nav-link" href="register">Register</a>
                        </li>
                    </ul>
                </div>
                <!-- Collapsible content -->

            </div>

        </nav>
        <!--/.Navbar-->