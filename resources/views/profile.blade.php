@extends('adminlte.master-nav')

@section('content')
<div>
    <div class="row">
        <div class="col-lg-12 ">      
            <div class="jumbotron top-cont bg-blue">
                <div class="photo-bg ">
                <img class="bg-prof mx-auto " src="{{asset('/adminlte/images/key.jpg')}}" alt="">
                <img class="pho-prof rounded-circle z-depth-2 mx-auto d-block" width="200px" alt="100x100" src="{{asset('/adminlte/images/mini.jpg')}}">
                <h2>Muhammad Rizky Syawali</h2>
                <a href="/edit-profile"> <h5>Edit</h5></a>
                </div>
            </div>
        </div>
    </div>

<div>
    <div class="row">
        <div class="col-6 mx-auto status">
            <div class="input  bg-dark">
                <form class="form-horizontal" action="/" method="POST">
                    @csrf
                    <h3>Post Your Status</h3>
                    <div class="input-group input-group-sm mb-0">
                      <input class="form-control input-lg" name="status" placeholder="Post Your Status">
                      <div class="input-group-append">
                        <button type="submit" class="btn btn-danger">Send</button>
                      </div>
                    </div>
                  </form>
            </div>
        </div>
    </div>
</div>
</div>

<div class="container content mt-3">
    <div class="row">
      <div div class="card-body mt-3">
      <div class="tab-content">
      <div class="active tab-pane" id="activity">
        <!-- Post -->
        <div class="post">
          <div class="user-block">
            <img class="img-circle img-bordered-sm" src="/adminlte/dist/img/user1-128x128.jpg" alt="user image">
            <span class="username">
              <a href="#">Jonathan Burke Jr.</a>
              <div class="btn-group float-right" role="group">
                <a class="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-h"></i>
                </a>
                <div class="dropdown-menu" id="show" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                  <a class="dropdown-item" href="#">Something else here</a>
                </div>
              </div>  
            </span>
            <span class="description">Shared publicly - 7:30 PM today</span>
          </div>
          <!-- /.user-block -->
          <p>
            Lorem ipsum represents a long-held tradition for designers,
            typographers and the like. Some people hate it and argue for
            its demise, but others ignore the hate as they create awesome
            tools to help create filler text for everyone from bacon lovers
            to Charlie Sheen fans.
          </p>

          <hr>
            <div class="panel-group">
              <div class="panel panel-default">
                <div class="panel-heading   ">
                   <p class="link-black text-sm mr-3 liketoggle" id="countButton"><i class="far fa-thumbs-up mr-1"></i>Like <span id="displayCount">(0)</span> </p>
                    <a class="link-black text-sm" data-toggle= "collapse" href="#collapse1"> <i class="far fa-comments mr-1"></i> Comments (5)</a>
                </div>
                <hr>
                <div id="collapse1" class="panel-collapse collapse mt-3 ">
                  <ul class="list-group ans ">
                    <span class="username">
                      <a href="#">Jonathan Burke Jr.</a>  
                    </span>
                    <span class="description">3 minute ago</span>
                     <p>Lorem ipsum represents a long-held tradition for designers,
                      typographers and the like. Some people hate it and argue for
                      its demise, but others ignore the hate as they create awesome
                      tools to help create filler text for everyone from bacon lovers
                      to Charlie Sheen fans.</p>
                  </ul>
                  <hr>
                  <ul class="list-group ans ">
                    <span class="username">
                      <a href="#">Jonathan Burke Jr.</a>  
                    </span>
                    <span class="description">3 minute ago</span>
                     <p>Lorem ipsum represents a long-held tradition for designers.</p>
                  </ul>
                  <hr>
                </div>
                
              </div>
            </div>
          <form class="form-horizontal">
            <div class="input-group input-group-sm mb-0">
              <input class="form-control form-control-sm" placeholder="Response">
              <div class="input-group-append">
                <button type="submit" class="btn btn-danger">Send</button>
              </div>
            </div>
          </form>
        </div>
        <!-- /.post -->
      </div>      
    </div>
    <!-- /.tab-content -->
  </div>
  </div>
</div>
<hr class="hr-split">


@endsection