<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostController@index');

Route::get('/login','AuthController@login');
Route::get('/register','AuthController@register');
Route::get('/profile','AuthController@profile');

Route::post('/','PostController@store');
Route::put('/','PostController@update');

Route::delete('/','PostController@destroy');

Route::resource('post', 'PostController');
